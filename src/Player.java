/* NOTE :
  Cette classe est incomplète : les commentaires sont là pour vous guider
 */

import java.util.*;

class Player {

    String name;
    CardPacket hiddenCards;
    CardPacket revealedCards;
    int id; // player id in the current Party, -1 if he didn't join a party.

    public Player(String name) {
        this.name = name;
        id = -1;
        hiddenCards = null; //will be set when joining party
        revealedCards = null; //will be set when joining party
    }

    public void joinParty(int id, List<Card> heap) {
        this.id = id;
        hiddenCards = new CardPacket(heap);
        revealedCards = new CardPacket();
    }

    public Card revealCard() {
        // enlever la première carte du tas caché
        Card carteEnlever = hiddenCards.removeFirst();
        // mettre cette carte en premier dans le tas révélé
        revealedCards.addFirst(carteEnlever);
        // renvoyer cette carte
        return carteEnlever;
    }

    public Card currentCard() {
        // si le tas révélé est vide renovyer null
        if(revealedCards.isEmpty())
        {
            return null;
        }
        // sinon renvoyer la première carte du tas révélé
        else
        {
            return revealedCards.get(0);
        }
    }

    public void takeCards(List<Card> heap)
    {
        // ajouter heap au tas caché
        hiddenCards.addCards(heap);
        // ajouter les cartes du tas révélé au tas caché
        revealedCards.addCards(hiddenCards);
        // vider le tas révélé
        revealedCards.clear();
        // mélanger le tas caché
        hiddenCards.shuffle();
    }

    public List<Card> giveRevealedCards() {
        List<Card> cards = new ArrayList<Card>();
        // mettre toutes les cartes du tas révélé dans cards
        cards.addAll((Collection<? extends Card>) revealedCards);
        // vider le tas révélé
        revealedCards.clear();
        // renvoyer cards
        return cards;
    }

    public boolean hasWon() {
        // renvoie true si tas révélé et caché sont vide
        if(revealedCards.isEmpty() && hiddenCards.isEmpty())
        {
            return true;
        }
        // sinon false
        else
        {
            return false;
        }
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }
}
