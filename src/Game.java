/* NOTE :
  Cette classe est incomplète : les commentaires sont là pour vous guider
 */


import java.util.ArrayList;
import java.util.List;

class Game {


    List<Party> parties; // all created parties
    List<Player> players; // all players connected to the server

    public Game() {
        parties = new ArrayList<Party>();
        players = new ArrayList<Player>();
    }

    public synchronized Party createParty(String name, Player creator, int nbPlayers) {
        Party p = null;
        // si partie déjà créée par creator, alors renvoyer null
        for(Party str:parties){
            if(str.name.equals(name)){
                return null;
            }
        }
        // sinon, créer une Party p et l'ajouter à parties
        p = new Party(parties.size()+1,name, creator, nbPlayers);
        parties.add(p);

        return p;
    }

    public synchronized Player createPlayer(String name) {
        Player p = null;
        // si player avec name copmme nom existe déjà, renvoyer null
        for(Player str:players){
            if(str.name.equals(name)){
                return null;
            }
        }
        // sinon, créer une Player p et l'ajouter à players
        p = new Player(name);
        players.add(p);

        return p;
    }

    public synchronized void removeParty(Party p) {
        // supprimer p de parties
        parties.remove(p);
    }

    public synchronized void removePlayer(Player p) {
        // supprimer p de players
        players.remove(p);
    }

    public synchronized boolean playerJoinParty(Player player, Party party) {
        // si le player n'est pas dans players, renvoyer false
        System.out.println("blublu");
        if(!players.contains(player)){
            System.out.println("blublu1");
            return false;
        }

        // si la party n'est pas dans parties, renvoyer false
        if(!parties.contains(party)){
            System.out.println("blublu2");
            return false;
        }

        // si l'id player == -1 (i.e. le joueur est déjà dans une partie), renvoyer false
        if(player.id != -1){
            System.out.println("blublu3");
            return false;
        }
        // sinon, ajouter player à party
        party.addPlayer(player);
       // party.addPlayer(player);
        System.out.println("coucou");
        return true;
    }
}
    
