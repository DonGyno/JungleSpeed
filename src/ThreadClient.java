/* NOTE :
  Cette classe est incomplète : les commentaires sont là pour vous guider
 */

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

class ThreadClient extends Thread {

    private static Random loto = new Random(Calendar.getInstance().getTimeInMillis());

    JungleIG ig;
    ObjectInputStream ois;
    ObjectOutputStream oos;
    Socket comm;
    ServerSocket sock;
    Boolean servready;

    public ThreadClient(JungleIG ig) {
        this.ig = ig;
        ois = ig.ois;
        oos = ig.oos;
    }

    public void run() {

        boolean stop = false;
        try {
            // recevoir booléen qui signale que le serveur est prêt
            System.out.println("Bonjour ICI");
            stop = ois.readBoolean();
            System.out.println("Bonjour TH CLIENT");
            // envoyer requête "attendre début partie"
            oos.writeInt(JungleServer.REQ_WAITPARTYSTARTS);
            oos.flush();
            System.out.println(JungleServer.REQ_WAITPARTYSTARTS);
            System.out.println("RQWPSTC");
            // recevoir mon id dans la partie
            int myIdPlayer = ois.readInt();

            while (!stop) {
                System.out.println("TH1");

                // envoyer requête "attendre début tour"
                oos.writeInt(JungleServer.REQ_WAITTURNSTARTS);
                // recevoir id joueur courant
                int idPlayerCurrent = ois.readInt();
                // si id joueur courant < 0, arreter thread
                if(idPlayerCurrent < 0) { Thread.currentThread().interrupt(); break; }
                // sinon si id joueur courant == mon id : afficher message dans ig du type "c'est mon tour"
                else if(idPlayerCurrent == myIdPlayer) { ig.textPlay.setText("C'est mon tour"); }
                // sinon afficher message dans ig du type "c'est le tour de X"
                else { ig.textPlay.setText("C'est le tour de "); } // lol

                // recevoir la liste des cartes visibles et les afficher dans l'ig
                List revealedcards = (List) ois.readObject();
                ig.textInfoParty.setText(String.valueOf(revealedcards));
                // débloquer le champ de saisie+bouton jouer
                ig.enableOrder(true);
                ig.enableOrder(true);
                // attendre 3s
                wait(3000);
                // bloquer le champ de saisie+bouton jouer
                ig.enableOrder(false);
                ig.enableOrder(false);
                // si pas d'rodre envoyé pdt les 3s
                if(ig.textPlay.getText()==""){
                    //    envoyer requête PLAY avec comme paramètre chaîne vide
                    oos.write(JungleServer.REQ_PLAY);
                    oos.writeBytes("");
                }
                // recevoir résultat du tour et l'afficher dans l'IG
                String result = ois.readUTF();
                ig.textInfoParty.setText(result);
                // recevoir booléen = true si partie finie, false sinon
                stop = ois.readBoolean();
            }
        }
        catch(IOException e) {}
        catch(ClassNotFoundException err )
            {
                System.err.println("Problème classe introuvable :" +err.getMessage());
            } catch (InterruptedException e) {
            e.printStackTrace();
        }
        JOptionPane.showMessageDialog(null, "Party is over. Return to main panel");
	ig.setInitPanel();
    }

}
