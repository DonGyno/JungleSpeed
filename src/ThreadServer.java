/* NOTE :
  Cette classe est incomplète : les commentaires sont là pour vous guider
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

class ThreadServer extends Thread {

    private static Random loto = new Random(Calendar.getInstance().getTimeInMillis());

    Socket comm;
    Game game;
    Player player;
    Party currentParty; // if null, player didn't join yet a party
    ObjectOutputStream oos;
    ObjectInputStream ois;
    boolean ok= false;


    public ThreadServer(Game game, Socket comm) {
        this.game = game;
        this.comm = comm;
        currentParty = null;
    }

    public void run() {
        String pseudo="";
        boolean ret = false;
        System.out.println("client connected.");
        try {
            //initialisation des flux objet
            ois = new ObjectInputStream(comm.getInputStream());
            oos = new ObjectOutputStream(comm.getOutputStream());
            //tant que pas ok
            while (!ok) {
                pseudo = (String) ois.readObject();
                System.out.println(pseudo);

                player = game.createPlayer(pseudo);
                if(player != null) {
                    oos.writeBoolean(true);
                }
                else {
                    oos.writeBoolean(false);
                }
                oos.flush();
                ok=true;
            }
        }
        catch(IOException e) {
            System.err.println("problem with client connection tttt. Aborting " + e.getMessage());
            return;
        }
        catch(ClassNotFoundException e) {
            System.err.println("problem with client request. Aborting");
            return;
        }

        try {
            while (true) {
                System.out.println("Bonjour");
                initLoop();
                System.out.println("Bonjour2");
                oos.writeBoolean(true); // synchro signals so that thread client does not sends to quickly a request
                System.out.println("Bonjour3");
                oos.flush();
                partyLoop();
                System.out.println("Bonjour4");

                // supprimer le flux sortant associé à player du pool de la partie courante
                currentParty.pool.removeStream(player.id);
                // ret = supprimer player de la partie courante
                // si ret == true (i.e. dernier joueur de la partie) supprimer la partie

                if(currentParty.nbPlayerInParty == 1){
                    currentParty.removePlayer(player);
                    game.removeParty(currentParty);
                }
                else{
                    currentParty.removePlayer(player);
                }
            }
        }
        catch(IllegalRequestException e) {
            System.err.println("client sent an illegal request: "+e.getMessage());
        }
        catch(IOException e) {
            System.err.println("pb with client connection2: "+e.getMessage());
        }
        // NB : si on arrive ici, c'est qu'il y a eu déconnexion ou erreur de requête

        // si partie courante != null (i.e. le joueur s'est déconnnecté en pleine partie)
        if(currentParty !=null){
            //    si l'état partie != en attente, etat partie = fin
            if(currentParty.state == Party.PARTY_WAITING){
                //    supprimer le flux sortant associé à player du pool de la partie courante
                currentParty.state = Party.PARTY_END;
            }
            //    ret = supprimer player de la partie courante
            currentParty.pool.removeStream(player.id);
            //    si ret == true (i.e. dernier joueur de la partie) supprimer la partie
            if(ret == true){
                game.removeParty(currentParty);
            }
        }
        // supprimer le joueur de game
        game.removePlayer(player);
    }

    public void initLoop() throws IllegalRequestException,IOException  {
        int idReq;
        boolean stop = false; // devient true en cas de requête CREATE ou JOIN réussie

        while (!stop) {
            // recevoir n° requete
            idReq = ois.readInt();
            // si n° correspond à LIST PARTY, CREATE PARTY ou JOIN PARTY appeler la méthode correspondante
            try{
                if(idReq == JungleServer.REQ_LISTPARTY){
                    requestListParty();
                }
                else if(idReq == JungleServer.REQ_CREATEPARTY){
                    requestCreateParty();
                }
                else if (idReq == JungleServer.REQ_JOINPARTY){
                    requestJoinParty();
                }
                // sinon générer une exception IllegalRequest
            }catch (IllegalRequestException e)
            {
                System.out.println("Erreur de requette" + e.getMessage());
            }
            stop = true;
        }
    }

    public void partyLoop() throws IllegalRequestException,IOException {

        int idReq;
        while (true) {
            // si etat partie == fin, retour
            /*if (currentParty.state == Party.PARTY_END) {
               return;
            }*/
            // recevoir n° requete
            System.out.println("PartyLoop");
            idReq = ois.readInt();
            System.out.println(idReq);
            // si etat partie == fin, retour
            /*if (currentParty.state == Party.PARTY_END) {
                return;
            }*/
            // si n° req correpsond à WAIT PARTY STARTS, WAIT TURN STARTS, PLAY, appeler la méthode correspondante
            try {
                if (idReq == JungleServer.REQ_WAITPARTYSTARTS) {
                    requestWaitPartyStarts();
                } else if (idReq == JungleServer.REQ_WAITTURNSTARTS) {
                    requestWaitTurnStarts();
                } else if (idReq == JungleServer.REQ_PLAY) {
                    requestPlay();
                }
                // sinon générer une exception IllegalRequest
            } catch (IllegalRequestException e) {
                System.err.println("Errueur de requette" + e.getMessage());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void requestListParty() throws IOException,IllegalRequestException {

        // traiter requete LIST PARTY (sans oublier les cas d'erreur)
        try {
            System.out.println("RQ LISTPARTIE");
            List liste = new ArrayList();

            for( Party p : game.parties){
                liste.add(p.creator.name);
                liste.add(p.name);
                liste.add(p.nbNeededPlayers);
                liste.add(p.idParty);
            }

            oos.writeObject(liste);
            oos.flush();
            System.out.println("RQ LISTPARTIE2");

        }catch(IOException e) {
            System.err.println("Problem with client connection: "+e.getMessage());
        }
    }

    public boolean requestCreateParty() throws IOException,IllegalRequestException {
        boolean rep = false; // mis a true si la requête permet effectivement de créer une nouvelle partie.
        // traiter requete CREATE PARTY (sans oublier les cas d'erreur)
        try {
            String nameParty = (String) ois.readObject();
            int nbPlayer = ois.read();
            Party p = game.createParty(nameParty, player, nbPlayer);
            if (!(p.equals(null))) {
                rep = true;
                oos.writeBoolean(true);
                oos.flush();
                currentParty = p;
            }
        }
        catch(IOException e) {
            System.err.println("pb with client connection: "+e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return rep;

        // NB : ne pas oublier d'ajouter le flux oos au pool de la partie créée
    }

    public boolean requestJoinParty() throws IOException,IllegalRequestException {
        boolean rep = false; // mis a true si la requête permet effectivement de rejoindre une partie existante
        // traiter requete JOIN PARTY (sans oublier les cas d'erreur
        try {
            System.out.println("blabla");
            String idPartie = (String)ois.readObject();
            System.out.println("blabla1");
            Party p = null;
            System.out.println("blabla2");
            for(int i =0; i< game.parties.size() ; i++){
                System.out.println("blabla3");
                if(Integer.parseInt(idPartie) == game.parties.get(i).idParty){
                    System.out.println("blabla4");
                    p = game.parties.get(i);
                    System.out.println("blabla5");
                }
            }
            System.out.println("blabla6");
            rep = game.playerJoinParty(player, p);
            System.out.println(rep);
            System.out.println("blabla7");
            oos.writeBoolean(true);
            System.out.println("blabla8");
            oos.flush();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // NB : ne pas oublier d'ajouter le flux oos au pool de la partie rejointe
        return rep;
    }

    public void requestWaitPartyStarts() throws IOException,IllegalRequestException {

        System.out.println(currentParty.name);
        // traiter requete WAIT PARY STARTS (sans oublier les cas d'erreur)
        currentParty.waitForPartyStarts();
        System.out.println("TEST");
    }

    public void requestWaitTurnStarts() throws IOException, IllegalRequestException, InterruptedException {

        Player currentPlayer;
        // traiter cas d'erreur
        // attendre début tour
        currentParty.waitForTurnStarts();
        // récupérer le joueur courant dans le tour -> currentPlayer
        currentPlayer = currentParty.currentPlayer;
        // si etat partie == fin, envoyer -1 au client sinon envoyer id joueur courant
        if(currentParty.state == Party.PARTY_END){
            oos.writeInt(-1);
        }
        else{
            oos.write(player.id);
        }

        // si je suis le thread associé au joueur courant
        long t = ThreadClient.currentThread().getId();
        if(currentPlayer.getId() == t){
            //    faire dodo entre 1 et 3s
            wait(3000);
            //    révéler une carte
            currentParty.revealCard();
            //    obtenir la liste des cartes visibles
            Object carteCourante = currentParty.getCurrentCards();
            //    mettre état partie à "joueur doivent jouer".
            currentParty.state = Party.PARTY_MUSTPLAY;
            //    envoyer cette liste à tous les clients (grâce au pool)
            currentParty.pool.sendToAll(carteCourante);
        }
    }

    public void requestPlay() throws IOException,IllegalRequestException {

        String action = "";
        int idAction = -1;
        boolean lastPlayed = false;

        // traiter cas d'erreur

        // recevoir la String qui indique l'ordre envoyé par le client
        action = ois.readUTF();
        // en fonction de cette String, initialiser idAction à ACT_TAKETOTEM ou ACT_HANDTOTEM ou ACT_NOP ou ACT_INCORRECT
        if(action == ""){
            idAction = JungleServer.ACT_NOP;
        }
        else if(action=="TT"){
            idAction = JungleServer.ACT_TAKETOTEM;
        }
        else if(action=="HT"){
            idAction = JungleServer.ACT_HANDTOTEM;
        }
        else {
            idAction = JungleServer.ACT_INCORRECT;
        }
        // lastPlayed <- intégrer l'ordre donné par le joueur (cf. integratePlayerOrder() )
        lastPlayed = currentParty.integratePlayerOrder(player,idAction);
        // si lastPLayer vaut true
        if(lastPlayed == true){
            //    si etat partie == fin
            if(currentParty.state == Party.PARTY_END){
                //       envoyer un message du style "partie finie" à tous les client
                currentParty.pool.sendToAll("Party END");
                //       envoyer true (= fin de partie) puyis retour
                oos.writeBoolean(true);
                return;
            }
            //    analyser les résultats
            currentParty.analyseResults();
            //    envoyer resultMsg de la partie courante à tous les clients
            currentParty.pool.sendToAll(currentParty.resultMsg);
            //    si etat partie == fin, envoyer true, sinon envoyer false
            if(currentParty.state == Party.PARTY_END){
                oos.writeBoolean(true);
            }
            else{
                oos.writeBoolean(false);
            }
        }
    }
}
